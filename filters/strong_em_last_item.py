def strong_em_last_item(str):
    str_parts = str.split(',')
    str_parts_length = len(str_parts)
    str_parts[str_parts_length - 1] = ''.join([
        '<strong><em>',
        str_parts[str_parts_length - 1],
        '</em></strong>'
    ])
    return ','.join(str_parts)
