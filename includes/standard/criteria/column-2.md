### {{ mobile-ads-heading[heading] <span>06</span> Mobile ads }} {: #mobile-ads }

#### {{ placement-req-heading[heading] PLACEMENT REQUIREMENTS }}

- {{ mobile-ads-1[list item] Static ad types (e.g. 6x1 banner and 1x1 tile ad) are allowed to be placed anywhere on the mobile page. }}
- {{ mobile-ads-2[list item] Small ads (6x1 banner or smaller) are allowed to be placed as a sticky ad on the bottom of the screen. Other formats are not allowed to stick. }}
- {{ mobile-ads-3[list item] Large ad types (e.g. native tile ads) are only allowed to be placed under the Primary Content.<sup aria-labelledby="large-ad"><a href="#large-ad">1</a></sup><sup aria-labelledby="primary-content"><a href="#primary-content">2</a></sup> }}

#### {{ size-req-heading[heading] SIZE REQUIREMENTS }}

{{ size-req-1 Ads showing on mobile screens are bound to the following size restrictions: }}

- {{ size-req-2[list item] Ads implemented on the scrollable portion of the webpage must not occupy in total more than 50 percent of the visible portion of the webpage.<sup aria-labelledby="standard-size"><a href="#standard-size">3</a></sup> }}
- {{ size-req-3[list item] Ads implemented as a ‘sticky ad’ have a maximum height restriction of 75px (or 15%). }}
- {{ size-req-4[list item] Below the Primary Content, ads are limited to 100% of the screen space.<sup aria-labelledby="below-primary"><a href="#below-primary">4</a></sup> }}

#### {{ animations-heading[heading] ANIMATIONS }}

{{ animations-1 Animations are allowed for the 6x1 ad type when placed as a ‘sticky’ ad on the bottom of the screen. Animations have to comply with the LEAN standard for animations, and a close button or some other closing mechanism must be included.<sup aria-labelledby="animations"><a href="#animations">5</a></sup> }}

### {{ other-formats-heading[heading] <span>07</span> Other Acceptable Ad formats? }}

{{ other-formats-1 Are your ads displayed on alternative screens, or are you convinced that you have an innovative Acceptable Ads format that doesn't fit the ads outlined above? [Let us know!](#){: data-mask='{"href": "bWFpbHRvOmFjY2VwdGFibGVhZHNAYWRibG9ja3BsdXMub3Jn"\}' } }}
