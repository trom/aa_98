# {{ standard-masthead-heading[heading] The Acceptable Ads Standard }}

{{ masthead-body-1 Every ad has to comply with the Acceptable Ads Standard, and participants cannot pay to avoid the criteria. eyeo acts on behalf of the Acceptable Ads Committee to enable publishers, ad networks, and ad-tech providers to participate in Acceptable Ads, and to enforce compliance with the criteria. }}

{{ masthead-body-2 For transparency, all Acceptable Ads are added to a [public forum](https://adblockplus.org/forum/viewforum.php?f=12) to allow the Internet community the opportunity to submit feedback. }}

{{ masthead-body-3 Users’ perspectives are important to us. If, for valid reasons, any Acceptable Ads proposal is rejected, the ad(s) will be excluded and thus not shown to ad-blocking users. }}

{{ masthead-body-4 Large entities pay a fee for eyeo’s services, which means participation in Acceptable Ads remains free for approximately 90% of participants. }}
