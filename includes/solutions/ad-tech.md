## {{ ad-tech-heading[heading] Ad tech }} {: .h1 }

{{ ad-tech-1 Offer a trusted and sustainable industry solution to provide additional value for your publishers and/or advertisers while respecting users’ choices. Our technical consultants can help you define the technical setup environment necessary to monetize ad-blocking traffic. }}
